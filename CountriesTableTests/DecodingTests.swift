import XCTest
@testable import WalmartCountries

final class DecodingTests: XCTestCase {

	let decoder = JSONDecoder()

    func testDecode() {
		do {
			let data = try XCTUnwrap(Self.getJSONFile("countries"))
			_ = try decoder.decode([Country].self, from: data)
		} catch {
			XCTAssertNil(error)
		}
    }

}

private extension DecodingTests {

	static func getJSONFile(_ name: String) -> Data? {
		guard let url = Bundle(for: Self.self).url(forResource: name, withExtension: "json") else { return nil }
		guard let data = try? Data(contentsOf: url) else { return nil }
		return data
	}

}
