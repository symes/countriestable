import Combine
import Foundation
import UIKit

final class CountryListViewController: UIViewController {

	private var cancellables = Set<AnyCancellable>()
	private let viewModel = CountryListViewModel()

	private let tableView = UITableView()
	private let searchController = UISearchController()
	private let activityIncidicator = UIActivityIndicatorView()

	override func viewDidLoad() {
		super.viewDidLoad()

		tableView.register(CountryTableCell.self, forCellReuseIdentifier: CountryTableCell.reuseID)
		tableView.dataSource = self
		tableView.translatesAutoresizingMaskIntoConstraints = false
		view.addSubview(tableView)
		setContentScrollView(tableView)
		NSLayoutConstraint.activate([
			tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			tableView.topAnchor.constraint(equalTo: view.topAnchor),
			tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
		])

		activityIncidicator.translatesAutoresizingMaskIntoConstraints = false
		view.addSubview(activityIncidicator)
		NSLayoutConstraint.activate([
			activityIncidicator.centerYAnchor.constraint(equalTo: view.centerYAnchor),
			activityIncidicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
		])

		searchController.searchResultsUpdater = self
		searchController.obscuresBackgroundDuringPresentation = false
		searchController.searchBar.placeholder = viewModel.searchBarPlaceholder
		navigationItem.searchController = searchController

		viewModel
			.countryList
			.sink { [weak self] _ in
				self?.tableView.reloadData()
			}
			.store(in: &cancellables)

		viewModel
			.$error
			.sink { [weak self] error in
				if let error {
					let alertController = UIAlertController(
						title: "Error",
						message: error.localizedDescription,
						preferredStyle: .alert
					)
					let cancelAction = UIAlertAction(
						title: "Cancel",
						style: .cancel,
						handler: { _ in
							self?.viewModel.error = nil
						}
					)
					alertController.addAction(cancelAction)
					self?.present(alertController, animated: true)
				}
			}
			.store(in: &cancellables)

		viewModel
			.$isLoading
			.sink { [weak self] isLoading in
				if isLoading {
					self?.activityIncidicator.startAnimating()
				} else {
					self?.activityIncidicator.stopAnimating()
				}
			}
			.store(in: &cancellables)

		Task {
			await viewModel.loadCountries()
		}
	}

}

extension CountryListViewController: UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return viewModel.countryList.value.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: CountryTableCell.reuseID, for: indexPath) as? CountryTableCell else { return .init() }
		cell.configure(viewModel.countryList.value[indexPath.row])
		return cell
	}

}

extension CountryListViewController: UISearchResultsUpdating {

	func updateSearchResults(for searchController: UISearchController) {
		var filter: String?
		if let text = searchController.searchBar.text, !text.isEmpty {
			filter = text
		}
		viewModel.setFilter(filter)
	}

}
