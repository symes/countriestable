import Combine
import Foundation

protocol APIProtocol {
	func getItems<T: Codable>(_ endpoint: APIEndpoint) async throws -> [T]
}

enum APIEndpoint {
	case countries

	var url: URL {
		switch self {
		case .countries:
			return URL(string: "https://gist.githubusercontent.com/peymano-wmt/32dcb892b06648910ddd40406e37fdab/raw/db25946fd77c5873b0303b858e861ce724e0dcd0/countries.json")!
		}
	}
}

class APIService: APIProtocol {

	enum APIError: Error {
		case invalidResponse(URLResponse)
	}

	private let urlSession: URLSession = .shared
	private let jsonDecoder: JSONDecoder = .init()
	private var cancellable: AnyCancellable? = nil

	func getItems<T: Codable>(_ endpoint: APIEndpoint) async throws -> T {
		var request = URLRequest(url: endpoint.url)
		request.setValue("application/json", forHTTPHeaderField: "Accept")
		let (data, response) = try await urlSession.data(for: request)

		// perform basic validation
		guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
			throw APIError.invalidResponse(response)
		}
		return try jsonDecoder.decode(T.self, from: data)
	}

}
