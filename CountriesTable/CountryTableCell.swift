import Foundation
import UIKit

final class CountryTableCell: UITableViewCell {

	static let reuseID = "country-cell"

	let nameRegionLabel: UILabel = {
		let label = UILabel()
		label.numberOfLines = 0
		label.font = UIFont.preferredFont(forTextStyle: .body)
		label.adjustsFontForContentSizeCategory = true
		return label
	}()

	let codeLabel: UILabel = {
		let label = UILabel()
		label.textAlignment = .right
		label.font = UIFont.preferredFont(forTextStyle: .body)
		label.adjustsFontForContentSizeCategory = true
		label.setContentCompressionResistancePriority(.required, for: .horizontal)
		return label
	}()

	let capitalLabel: UILabel = {
		let label = UILabel()
		label.adjustsFontSizeToFitWidth = true
		label.font = UIFont.preferredFont(forTextStyle: .body)
		label.adjustsFontForContentSizeCategory = true
		return label
	}()

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)

		[nameRegionLabel, codeLabel, capitalLabel].forEach {
			contentView.addSubview($0)
			$0.translatesAutoresizingMaskIntoConstraints = false
		}
		let margin: CGFloat = 16
		NSLayoutConstraint.activate([
			nameRegionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: margin),
			nameRegionLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: margin / 2),
			nameRegionLabel.bottomAnchor.constraint(equalTo: capitalLabel.topAnchor),
			nameRegionLabel.trailingAnchor.constraint(equalTo: codeLabel.leadingAnchor, constant: -margin / 2),
			nameRegionLabel.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -margin / 2),

			codeLabel.topAnchor.constraint(equalTo: nameRegionLabel.topAnchor),
			codeLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -margin),

			capitalLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: margin),
			capitalLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -margin / 2),
			capitalLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -margin),
		])
	}

	func configure(_ country: Country) {
		nameRegionLabel.text = country.nameRegion
		codeLabel.text = country.code.rawValue
		capitalLabel.text = country.capital
	}

	override func prepareForReuse() {
		super.prepareForReuse()
		nameRegionLabel.text = ""
		codeLabel.text = ""
		capitalLabel.text = ""
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

}

private extension Country {

	var nameRegion: String {
		guard let region else { return name }
		return name + ", " + (region.rawValue)
	}

}
