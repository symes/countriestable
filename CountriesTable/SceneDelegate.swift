import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

	var window: UIWindow?

	func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
		guard let windowScene = (scene as? UIWindowScene) else { return }
		window = UIWindow(windowScene: windowScene)
		let countryListViewController = CountryListViewController()
		let navigationController = UINavigationController(rootViewController: countryListViewController)
		window?.rootViewController = navigationController
		window?.makeKeyAndVisible()
	}

	func sceneDidDisconnect(_ scene: UIScene) {
		// Intentionally Left Blank
	}

	func sceneDidBecomeActive(_ scene: UIScene) {
		// Intentionally Left Blank
	}

	func sceneWillResignActive(_ scene: UIScene) {
		// Intentionally Left Blank
	}

	func sceneWillEnterForeground(_ scene: UIScene) {
		// Intentionally Left Blank
	}

	func sceneDidEnterBackground(_ scene: UIScene) {
		// Intentionally Left Blank
	}

}

