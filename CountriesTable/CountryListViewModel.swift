import Combine
import Foundation

@MainActor
class CountryListViewModel {

	var countryList: CurrentValueSubject<[Country], Never> = .init([])
	private var allCountries: [Country] = []
	private var filterTerm: String?
	@Published var isLoading = false
	@Published var error: Error? = nil

	let apiService: APIProtocol

	init(apiService: APIProtocol = APIService()) {
		self.apiService = apiService
	}

	func loadCountries() async {
		isLoading = true
		do {
			allCountries = try await apiService.getItems(.countries)
			countryList.value = filterCountries(filterTerm, countries: allCountries)
			isLoading = false
		} catch {
			isLoading = false
			self.error = error
		}
	}

	func filterCountries(_ filter: String?, countries: [Country]) -> [Country] {
		guard let filter else {
			return countries
		}
		return countries.filter { $0.name.contains(filter) || $0.capital.contains(filter) }
	}

	func setFilter(_ filter: String?) {
		filterTerm = filter
		countryList.value = filterCountries(filter, countries: allCountries)
	}

	var searchBarPlaceholder: String {
		"Search by Country or Capital"
	}

}
